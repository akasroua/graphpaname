{ pkgs ? import <nixpkgs> { } }:

with pkgs.python37Packages;

pkgs.mkShell {
  buildInputs = [
    # Dependencies
    pandas
    requests
    flask
    flask-bootstrap
    flask_wtf
    folium
    pytest
    beautifulsoup4
  ];
}
