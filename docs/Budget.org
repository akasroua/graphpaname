#+TITLE: Presupuesto de graphPaname
#+AUTHOR: Amin Kasrou Aouam, Alejandro Calle González-Valdés
#+LANGUAGE: es
* Presupuesto de graphPaname

El objetivo de nuestro proyecto es crear un sistema de información que permita
visualizar diversos datos de interés, de una Smart City.
Procedemos a desglosar a lo largo de este documento, el presupuesto estimado
para poner en marcha este proyecto.
Hemos decidido realizar 2 presupuestos, uno para el desarrollo y otro para el
despliegue, con el fin de compartimentar cada gasto en la fase del proyecto.

** Desarrollo

La fase de desarrollo es la más intensiva, requiere más personal y dispositivos
para conseguir un buen resultado.

*** Personal

No precisamos de una gran plantilla, pero sí de especialistas en muchos ámbitos.
Un equipo multidisciplinar permite la paralelización de la mayoría de tareas,
además de una mayor eficiencia, debido a la especialización.

| Puesto            | Número de empleados | Salario mensual | Duración |  Total |
| Desarrollador     |                   2 |            2500 | 3 años   |        |
| DevOps            |                   1 |            1700 | 3 años   |        |
| SysAdmin          |                   1 |            2000 | 3 años   |        |
| Community Manager |                   1 |            1500 | 3 años   |        |
| Diseñador Gráfico |                   1 |            1200 | 3 años   |        |
| Total             |                     |                 |          | 318900 |

*** Ejecución

Los trabajos remotos son cada vez más populares, en algunos ámbitos casi todas
las ofertas son de ese tipo.
Como nuestro equipo es pequeño, y no necesitan trabajar en el mismo turno,
podemos aprovechar esta característica, y ejercer nuestra labor de forma remota.
Por consecuente, requerimos de infraestructura tecnológica.

| Destinatario      | Cantidad | Dispositivo                   |  Precio | Referencia |
| Desarrollador     |        2 | Lenovo Thinkpad L390          |  825.59 | [[https://www.pccomponentes.com/lenovo-thinkpad-l390-intel-core-i5-8265u-8gb-256gb-ssd-133][Enlace]]     |
| DevOps            |        1 | Lenovo Ideapad C340-14API     |     599 | [[https://www.pccomponentes.com/lenovo-ideapad-c340-14api-amd-ryzen-5-3500u-8gb-256gb-ssd-14-tactil][Enlace]]     |
| SysAdmin          |        1 | Lenovo Ideapad C340-14API     |     599 | [[https://www.pccomponentes.com/lenovo-ideapad-c340-14api-amd-ryzen-5-3500u-8gb-256gb-ssd-14-tactil][Enlace]]     |
| Community Manager |        1 | Lenovo Ideapad C340-14API     |     599 | [[https://www.pccomponentes.com/lenovo-ideapad-c340-14api-amd-ryzen-5-3500u-8gb-256gb-ssd-14-tactil][Enlace]]     |
| Diseñador Gráfico |        1 | Apple MacBook Air             |     889 | [[https://www.pccomponentes.com/apple-macbook-air-intel-core-i5-8gb-128gb-ssd-13-plateado][Enlace]]     |
| Equipo            |        1 | WD My Cloud EX2 Ultra 8TB NAS |  429.15 | [[https://www.pccomponentes.com/wd-my-cloud-ex2-ultra-8tb-nas][Enlace]]     |
| Total             |          |                               | 4866,33 |            |

Además de la infraestructura, precisamos de ayuda para facilitar las tareas burocráticas.

| Gastos administrativos | Precio | Duración | Total |
| Consuloría legal       |     70 | 3 años   |       |
| Auditoría de seguridad |    129 | -        |       |
| Total                  |        |          |  2649 |

*** Complementarios

La formación continua es uno de los pilares de la especialización, permite que
nuestros profesionales estén siempre al día y conozcan las metodologías más
adecuadas para cada caso de uso.

| Destinatario  | Cantidad | Curso de formación                       | Precio | Referencia |
| DevOps        |        1 | Kubernetes +Swarm From a Docker Captain  |     13 | [[https://www.udemy.com/course/docker-mastery/][Enlace]]     |
| Desarrollador |        2 | Machine Learning: Data Science in Python |     36 | [[https://www.udemy.com/course/machinelearningpython/][Enlace]]     |
| Total         |          |                                          |     49 |            |

Un producto sin promoción no facilita el éxito de éste. La inversión en
publicidad y campañas de /marketing/ es esencial.

| Plataforma | Cuota mensual | Duración | Total | Referencia |
| Facebook   | 100€          | 6 meses  |   600 | [[https://www.facebook.com/business/help/1844835042445690?id=629338044106215][Enlace]]     |
| Twitter    | 100           | 6 meses  |   600 | [[https://business.twitter.com/en/help/campaign-setup/campaign-dates-and-budget.html][Enlace]]     |
| Total      |               |          |  1200 |            |

** Despliegue

La fase de despliegue requiere más trabajo para nuestros SysAdmin y DevOps, y el
coste mensual se puede alterado por muchas variables.

*** Sistema in house

Elegiremos un servidor de gama media, con capacidad suficiente para los
usuarios, y posibilidad de expansión para el futuro.

| Elemento                 | Precio | Duración | Referencia | Total |
| Thinksystem ST50         |    578 | -        | [[https://www.lenovo.com/us/en/data-center/servers/towers/ThinkSystem-ST50/p/77XX7TRST51][Enlace]]     |       |
| Gastos de funcionamiento |     40 | 3 años   |            |       |
| Total                    |        |          |            |  2018 |

*** Sistema Cloud Computing

Como proveedor elegimos Amazon Web Services, dado que ofrece diversos servicios,
que se pueden combinar entre ellos para obtener una solución adaptada a cada problema.

| Instancias | Tipo     | Características | Cuota mensual | Duración | Total |
| 4          | t2.micro | EBS-Backed      |            15 | 3 años   |       |
| Total      |          |                 |               |          |   540 |

El precio ha sido calculado mediante una estimación de los créditos de CPU que
necesita nuestra sistema para funcionar con normalidad. Nos hemos basado en un
proyecto en el que participó uno de nuestros integrantes, [[https://odyfosports.com][Odyfo]].
