DATASETS = [
    "coronavirus-commercants-parisiens-livraison-a-domicile",
    "deconfinement-pistes-cyclables-temporaires",
    "deconfinement-parking-relais-doublement-des-places",
    "deconfinement-rues-amenagees-pour-pietons",
]
DATASET_URL = "https://opendata.paris.fr/api/records/1.0/search/?dataset={}&q=&rows=-1"
FLICKR_URL = "https://www.flickr.com/search/?text={}"
COLUMNS = {
    "deconfinement-pistes-cyclables-temporaires": [
        "fields.geo_shape.type",
        "fields.geo_shape.coordinates",
        "fields.statut",
        "record_timestamp",
        "fields.complement",
    ],
    "deconfinement-parking-relais-doublement-des-places": [
        "fields.societe",
        "fields.nb_places_dispositif_environ",
        "fields.parcs",
        "fields.geo_shape.type",
        "fields.geo_shape.coordinates",
        "fields.cp",
        "fields.ville",
        "fields.adresse",
    ],
    "coronavirus-commercants-parisiens-livraison-a-domicile": [
        "fields.geo_shape.type",
        "fields.geo_shape.coordinates",
        "fields.adresse",
        "fields.code_postal",
        "fields.nom_du_commerce",
        "fields.type_de_commerce",
        "fields.site_internet",
        "record_timestamp",
        "fields.precisions",
        "fields.telephone",
        "fields.mail",
    ],
    "deconfinement-rues-amenagees-pour-pietons": [
        "fields.geo_shape.type",
        "fields.geo_shape.coordinates",
        "fields.nom_voie",
        "fields.categorie",
        "fields.statut",
        "record_timestamp",
    ],
}
CHOICES = [
    ("coronavirus-commercants-parisiens-livraison-a-domicile", "home-delivery"),
    ("deconfinement-pistes-cyclables-temporaires", "cycling-paths"),
    ("deconfinement-parking-relais-doublement-des-places", "relay-parking"),
    ("deconfinement-rues-amenagees-pour-pietons", "pedestrian-streets"),
]
SECRET_KEY = "trolaso"
COORDINATES = [48.864716, 2.349014]
