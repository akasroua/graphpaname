from pandas import DataFrame
from requests import get

from app.preprocessing import create_dataframe
from app.data_request import request_dataset
from constants import COLUMNS, DATASETS, DATASET_URL, FLICKR_URL


def test_dataset_request():
    """
    Checks that the datasets URLs are reachable
    """
    for dataset in DATASETS:
        response = get(DATASET_URL.format(dataset))
        assert response.status_code == 200


def test_dataframe_creation():
    """
    Verifes that the DataFrames are created and filtered properly
    """
    for dataset in DATASETS:
        df = create_dataframe(dataset)
        assert isinstance(df, DataFrame)
        assert all(df.columns == COLUMNS[dataset])


def test_flickr_request():
    """
    Checks that Flickr search is avalaible
    """
    response = get(FLICKR_URL.format("paris coronavirus"))
    assert response.status_code == 200
