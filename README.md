graphPaname
===========

graphPaname is a system that collects real-time data, relevant to the
COVID-19 pandemic de-escalation, from the city of Paris.

It works with 4 datasets about the de-escalation:

-   Retailers with home delivery
-   Additional parking places in relay parkings (parkings connected to
    public transportation)
-   Temporary cycling paths
-   Temporary pedestrian streets

For each dataset, we offer a table with the data, and a map of Paris
with markers. Additionally, there\'s a section with photos related to
the COVID-19 pandemic.

Technologies
------------

-   Flask
-   Pandas
-   Folium

Data sources
------------

-   [Open Data](https://opendata.paris.fr/pages/home/)
-   [OpenStreetMap](https://www.openstreetmap.org/)
-   [Flickr](https://flickr.com)

Requirements
------------

-   Nix

Installation
------------

1.  Install Nix (compatible with MacOS and Linux):

``` {.shell}
curl -L https://nixos.org/nix/install | sh
```

There are alternative installation methods, if you don\'t want to pipe
curl to sh

2.  Clone the repository:

``` {.shell}
git clone https://coolneng.duckdns.org/gitea/coolneng/graphPaname
```

3.  Change the working directory to the project:

``` {.shell}
cd graphPaname
```

4.  Enter the nix-shell:

``` {.shell}
nix-shell
```

5.  Run the tests:

``` {.shell}
pytest
```

6.  Execute the Flask application:

``` {.shell}
flask run
```

The website can be accessed via **localhost:5000**
