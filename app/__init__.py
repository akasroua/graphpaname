from flask import Flask

from constants import SECRET_KEY
from flask_bootstrap import Bootstrap

app = Flask(__name__)
app.secret_key = SECRET_KEY
app.config['TEMPLATES_AUTO_RELOAD'] = True
bootstrap = Bootstrap(app)

from app import errors, routes
