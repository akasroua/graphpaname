from flask import render_template

from app import app
from app.forms import DatasetForm
from app.processing import process_data
from app.data_request import scrape_flickr


@app.route("/")
@app.route("/index")
def index():
    return render_template("index.html", title="Home Page")


@app.route("/data", methods=["GET", "POST"])
def data():
    form = DatasetForm()
    if form.validate_on_submit():
        table = process_data(form.dataset.data)
        return render_template("visualization.html", title="Visualization", table=table)
    return render_template("data.html", title="Data", form=form)


@app.route("/visualization")
def visualization():
    return render_template("visualization.html", title="Visualization", table=table)


@app.route("/map")
def map():
    return render_template("map.html")


@app.route("/photos")
def photos():
    images = scrape_flickr("paris coronavirus")
    return render_template("photos.html", title="Photos", images=images)
