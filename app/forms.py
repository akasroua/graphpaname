from constants import CHOICES
from flask_wtf import FlaskForm
from wtforms import SelectField, SubmitField


class DatasetForm(FlaskForm):
    """
    Web form to select a dataset
    """

    dataset = SelectField(choices=CHOICES)
    submit = SubmitField("Submit")
