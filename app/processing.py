from app.preprocessing import create_dataframe, create_map


def create_table(df) -> str:
    """
    Renders an HTML table from a DataFrame
    """
    df.fillna(value=0, inplace=True)
    table = df.to_html(classes=["table-striped", "table-sm", "table-responsive"])
    return table


def process_data(dataset):
    """
    Creates the DataFrame, produces a map and returns a table
    """
    df = create_dataframe(dataset)
    table = create_table(df)
    create_map(df)
    return table
